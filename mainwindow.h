#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui/QWidget>
#include "ui_mainwindow.h"
#include <QWebView>
#include <QStandardItemModel>

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0, Qt::WFlags flags = 0);
    ~MainWindow();

public slots:
    void on_tagButton_clicked();
    void on_clearButton_clicked();
    void on_zoomUser_clicked();
    void on_zoomGlobal_clicked();

private:
    Ui::MainWindowClass ui;
    bool enableUIFlag;
};

#endif // MAINWINDOW_H
