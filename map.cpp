#include "map.h"

#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QDomDocument>
#include <QDomElement>
#include <QWebFrame>
#include <QWebPage>
#include <QEventLoop>
#include <QApplication>
#include <QFile>
#include <QChar>
#include <QMessageBox>
#include <QDebug>

#include <math.h>
#include <iostream>

Map::Map(QWidget *parent) : QWebView(parent), pendingRequests(0)
{
    manager = new QNetworkAccessManager(this);
    connect(manager, SIGNAL(finished(QNetworkReply*)),
            this, SLOT(replyFinished(QNetworkReply*)));
    connect(this,SIGNAL(reloadMap()),
            this,SLOT(loadCoordinates()));

    zoomingLevel = 15;
    zoomingMode = 0; // zooming mode: overview
    userCoordinates.clear();


    //load(QUrl(QFile("index.html")));
}

void Map::geoCode(const QString& address)
{
    addUser(address);
    QString requestStr( tr("http://maps.google.com/maps/geo?q=%1&output=%2&key=%3")
            .arg(address)
            .arg("csv")
            .arg("GOOGLE_MAPS_KEY") );

    manager->get( QNetworkRequest(requestStr) );
    ++pendingRequests;
}

void Map::replyFinished(QNetworkReply *reply)
{
    /*
    QString replyStr( reply->readAll() );
    QStringList coordinateStrList = replyStr.split(",");

    if( coordinateStrList.size() == 4) {
        QPointF coordinate( coordinateStrList[2].toFloat(),coordinateStrList[3].toFloat() );
        userCoordinates << coordinate;
    }
    */

    --pendingRequests;
    if( pendingRequests<1 )
        emit( reloadMap() );
}

void Map::loadCoordinates()
{
    QStringList scriptStr;
    scriptStr
            << "var map = new GMap2(document.getElementById(\"map\"));"
            << "var bounds = new GLatLngBounds;"
            << "var markers = [];";

    QString zoomingString; // << "map.setCenter( new GLatLng(0,0),1 );";
    //zoomingString = QString("map.setCenter( new GLatLng(0,0),%1 );").arg(zoomingLevel);
    zoomingString = zoomingModeString();
    qDebug("Zooming mode: %d", zoomingMode);
    scriptStr << zoomingString;

    scriptStr
            << "var misterXIcon = new GIcon(G_DEFAULT_ICON);"
            << "misterXIcon.image = \"http://www.google.com/mapfiles/markerX.png\";"
            << "misterXMarker = { icon:misterXIcon };"
            << "var userIcon = new GIcon();"
            << "userIcon.image = \"http://labs.google.com/ridefinder/images/mm_20_blue.png\";"
            << "userIcon.shadow = \"http://labs.google.com/ridefinder/images/mm_20_shadow.png\";"
            << "userIcon.iconSize = new GSize(12, 20);"
            << "userIcon.shadowSize = new GSize(22, 20);"
            << "userIcon.iconAnchor = new GPoint(6, 20);"
            << "userIcon.infoWindowAnchor = new GPoint(5, 1);"
            << "userMarker = { icon:userIcon };";


    // first point in userCoordinates is MisterX, therefore we assign a special icon to it
    // TODO: check if message ID == MisterX id, then redraw Map
    if (!userCoordinates.isEmpty())
    {
        scriptStr << QString("markers[0] = new GMarker(new GLatLng(%1, %2), misterXMarker);")
                             .arg(userCoordinates[0].x())
                             .arg(userCoordinates[0].y());

        int num=0;
        foreach (QPointF point, userCoordinates)
        {
            scriptStr << QString("markers[%1] = new GMarker(new GLatLng(%2, %3), userMarker);")
                                 .arg(++num)
                                 .arg(point.x())
                                 .arg(point.y());

                                 //std::cout << "Point X: " << point.x() << std::endl;
                                //std::cout << "Point Y: " << point.y() << std::endl;
        }

    }

    scriptStr
            << "for( var i=0; i<markers.length; ++i ) {"
            << "   bounds.extend(markers[i].getPoint());"
            << "   map.addOverlay(markers[i]);"
            <<    "}"
            <<    "map.setCenter(bounds.getCenter());";

            //<< "map.addControl(new GLargeMapControl());" // for fadenkreuz for big touchscreens


    this->page()->mainFrame()->evaluateJavaScript( scriptStr.join("\n") );
}

void Map::clearCoordinates()
{
    userCoordinates.clear();
}


void Map::addUser(const QString& address)
{
    QString str = address;
    QStringList strlist = str.split(",", QString::SkipEmptyParts);
    //QString floatX = "";
    //QString floatY = ""; // the rest of the string is Y-coord
    //QChar *data = str.data();
    //int i = 0;

    if (str.contains(","))
    {


        float pointX = strlist[0].toFloat();
        float pointY = strlist[1].toFloat();
        userCoordinates.append(QPointF(pointX, pointY));
    }
    else
    {
        QMessageBox::information(this, "Please type the geoCode in the correct format!", "Example: 47.123456, 8.601020");
    }

    zoomingMode = 1;

}



void Map::zoomIn()
{
    ++zoomingLevel;
}



void Map::zoomOut()
{
    --zoomingLevel;
}


int Map::showZoomLevel()
{
    return zoomingLevel;
}

void Map::setZoomingMode(int zoomingModeToSet)
{
    zoomingMode = zoomingModeToSet;
    std::cout << "zooming mode set to: " << zoomingMode << endl;

}


QString Map::zoomingModeString()
{
    if (zoomingMode == 1)
    {
        qDebug("in map mode: zoom-user");
        return QString("map.setCenter( new GLatLng(%1,%2),%3 );").arg(userCoordinates[0].x()).arg(userCoordinates[0].y()).arg(zoomingLevel);
    }
    else // zoomingMode = 0 assumed && for errorhandling
    {
        qDebug("in map mode: overview");
        return QString("map.setCenter( new GLatLng(%1,%2),%3 );").arg(0).arg(0).arg(0);

    }



}





