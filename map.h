#ifndef HEADER_H
#define HEADER_H

#include <QWebView>

class QNetworkAccessManager;

class Map : public QWebView
{
    Q_OBJECT

    public:
        Map(QWidget *parent=0);
        QList<QPointF> userCoordinates;


public slots:
        void replyFinished(QNetworkReply*);
        void loadCoordinates();
        void geoCode(const QString &address);
        void clearCoordinates();
        void addUser(const QString &address);
        void zoomIn();
        void zoomOut();
        int showZoomLevel();
        void setZoomingMode(int zoomingModeToSet);
        QString zoomingModeString();


signals:
        void reloadMap();

private:
        QNetworkAccessManager *manager;

        int pendingRequests;
        int zoomingLevel;
        int zoomingMode;
};

#endif // HEADER_H
