#include "mainwindow.h"
#include <QVBoxLayout>
#include <QDir>
#include <QWebView>
#include <QHeaderView>
#include <QMessageBox>
#include <QStringListModel>
#include <QStringList>

#include "map.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent, Qt::WFlags flags)
    : QWidget(parent, flags)
{
    ui.setupUi(this);
    ui.map->show();
    ui.map->lower();
    ui.inputCoord->raise();
}

MainWindow::~MainWindow()
{

}


void MainWindow::on_tagButton_clicked()
{
     //ui.map->addUser(ui.inputCoord->text());
     ui.map->geoCode(ui.inputCoord->text());
     //qDebug() << ui.inputCoord->text();
}

void MainWindow::on_clearButton_clicked()
{
    if (enableUIFlag)
    {
         ui.map->clearCoordinates();
         ui.clearButton->setText(QString("Enable"));
         ui.inputCoord->hide();
         ui.zoomUser->hide();
         ui.zoomGlobal->hide();
         ui.tagButton->hide();
         enableUIFlag = 0;
         //ui.map->geoCode("47.3702, 8.544");
    }
    else
    {
        ui.clearButton->setText(QString("Disable"));
        ui.inputCoord->show();
        ui.zoomUser->show();
        ui.zoomGlobal->show();
        ui.tagButton->show();
        enableUIFlag = 1;
    }
}

void MainWindow::on_zoomUser_clicked()
{
        ui.map->setZoomingMode(1);
        ui.map->loadCoordinates();
}


void MainWindow::on_zoomGlobal_clicked()
{
    ui.map->setZoomingMode(0);
    ui.map->loadCoordinates();
}


